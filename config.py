
class Config(object):
    DEBUG = False
    TESTING = False
    # LOG_FILE = ../log
    # LOG_LEVEL = DEBUG
    PORT = 5000
    SECRET_KEY = 'secret key to store password'
    ODOO_SERVER = 'odoo'
    ODOO_PORT = '8069'
    ODOO_DB = 'admin'
